#!/bin/bash

NEWLOC=`curl -L "https://mediaarea.net/en/MediaInfo/Download/Mac_OS" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' 2>/dev/null | grep '.dmg' | head -1 | awk -F'"' '{print $2}' | tr -d '\r'`

if [ "x${NEWLOC}" != "x" ]; then
	echo "https:${NEWLOC}"
fi
